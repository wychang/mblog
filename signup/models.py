from django.db import models

# Create your models here.

class data_demo(models.Model):
    bigint_f = models.BigIntegerField()
    bool_f = models.BooleanField()
    date_f = models.DateField(auto_now=True)
    char_f = models.CharField(max_length=20)
    datatime_f = models.DateTimeField(auto_now_add=True)
    decimal_f = models.DecimalField(max_digits=10, decimal_places=2)
    float_f = models.FloatField(null=True)
    int_f = models.IntegerField(default=2020)
    text_f = models.TextField()

class registration_info(models.Model):
    dept_select = (
        ('IT', '資訊課'),
        ('HR', '人資課'),
        ('RD', '研發課'),
        ('PR', '公關課')
    )
    uid = models.CharField(max_length=20, unique=True, verbose_name='帳號')
    pwd = models.CharField(max_length=128, verbose_name='密碼')
    email= models.EmailField(verbose_name='電子信箱')
    signup_time = models.DateTimeField(auto_now_add=True)
    last_login = models.DateTimeField(auto_now=True)
    id_no = models.CharField(max_length=10, verbose_name='身分證字號')
    cellphone = models.CharField(max_length=10, verbose_name='手機')
    address = models.TextField(verbose_name='住址')
    webpage = models.URLField(verbose_name='個人網頁')
    dept = models.CharField(max_length=2, default="IT" , choices = dept_select, verbose_name='隸屬部門')
    st = models.BooleanField(verbose_name='帳號停權')
 
    def __str__(self):
        return self.uid

class product(models.Model):
    name = models.CharField(max_length=100)
    price = models.IntegerField()
    qty = models.IntegerField()

    def __str__(self):
        return self.name