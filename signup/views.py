from django.shortcuts import render
from django.http import HttpResponse,Http404
from signup.models import product

# Create your views here.
def about(request):
    html = '''
<!DOCTYPE html>
<html><head><title>About Myself</title></head>
<body><h2>Min-Huang Ho</h2><hr>
<p>Hi, I am Min-Huang Ho. Nice to meet you!</p>
</body>
</html>
'''
    return HttpResponse(html) 


def listing(request):
    html = '''
<!DOCTYPE html>
<html>
<head>
<meta charset='utf-8'>
<title>中古機列表</title>
</head>
<body>
<h2>以下是目前本店販售中的二手機列表</h2>
<hr>
<table width=400 border=1 bgcolor='#ccffcc'>
{}
</table>
</body>
</html>
'''
    products = product.objects.all()
    tags = '<tr><td>品名</td><td>售價</td><td>庫存量</td></tr>'
    for p in products:
        tags = tags + '<tr><td>{}</td>'.format(p.name)
        tags = tags + '<td>{}</td>'.format(p.price)
        tags = tags + '<td>{}</td></tr>'.format(p.qty)
    return HttpResponse(html.format(tags))

def disp_detail(request, id):
    try:
        p = product.objects.get(id=id)
    except product.DoesNotExist:
        raise Http404('找不到指定的品項')
    return render(request, "disp.html", locals())

