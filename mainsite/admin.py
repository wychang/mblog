from django.contrib import admin
from .models import Post
from signup.models import data_demo,registration_info, product

# Register your models here.
class PostAdmin(admin.ModelAdmin):
    list_display = ('title','slug','pub_date')

admin.site.register(Post,PostAdmin)
admin.site.register(data_demo)
admin.site.register(registration_info)
admin.site.register(product)
