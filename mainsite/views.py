from django.shortcuts import render
from django.http import HttpResponse
from .models import Post
from datetime import datetime


# Create your views here.
# def homepage(request):
    # posts = Post.objects.all()
    # post_lists = list()
    # for count, post in enumerate(posts):
    #     post_lists.append("No.{}:".format(str(count)) + str(post)+"<br>")
    #     post_lists.append("<small>"+str(post.body)+"</small><br><br>")
    # Create your views here.
    # return HttpResponse(post_lists)
def homepage(request):
    posts = Post.objects.all()
    now = datetime.now()
    return render(request, 'index.html', locals())
    
def showpost(request, slug):
    try:
        post = Post.objects.get(slug = slug)
        if post != None :
            return render(request, 'post.html', locals())
    except:
        return redirect('/')

def author(request):
    dom="<h2> Here is author page</h2><hr>"
    return HttpResponse(dom) 

def about1(request,author_id="zozo123456"):
    dom= "<h2> Here is Author: {}'s about page</h2><hr>".format(author_id)
    return HttpResponse(dom)

def post_times(request, yr, mon, day, post_num):
    dom = "<h2> {}/{}/{}:Post #{}</h2><hr>".format(yr,mon,day,post_num)
    return HttpResponse(dom)

   
